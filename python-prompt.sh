trim() {
    local var="$*"
    # remove leading whitespace characters
    var="${var#"${var%%[![:space:]]*}"}"
    # remove trailing whitespace characters
    var="${var%"${var##*[![:space:]]}"}"
    printf '%s' "$var"
}

shortest()
{
    # Returns the shortest of two strings
    if [[ ${#1} -lt ${#2} ]]
    then
        printf "%s" "$1"
    else
        printf "%s" "$2"
    fi
}

python_prompt()
{
    local relative venvpath pyversion _

    venvpath="$(which python 2>/dev/null)"
    venvpath="${venvpath%/bin/python}"

    if [[ -z "$venvpath" ]] || [[ "$venvpath" == "/usr" ]]; then
        return
    fi

    relative="$(realpath --relative-to="$(pwd)" -s "$venvpath")"
    venvpath="$(shortest "$venvpath" "$relative")"

    if [[ "$venvpath" == ".venv" ]]; then
        venvpath=""  # No need to tell me my venv is in .venv, I can guess it.
    fi

    read -r _ pyversion <<<"$(python --version)"

    printf "$1" "$(trim "$venvpath $pyversion")"
}
